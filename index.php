<?php

$interventionRequest = "SELECT id, nom_client, date_inter, intitule FROM intervention WHERE date_inter >= :date ORDER BY date_inter";
$employeRequest = "SELECT e.nom, e.prenom FROM employe AS e INNER JOIN inter_emp AS ie on e.id = ie.id_emp WHERE ie.id_inter = :id_inter";

require 'includes/validation.php';
require 'includes/connect.php';

$now = date('Y-m-d H:i:s');

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Gestionnaire de prise de rendez-vous - Garage Attens</title>
</head>
<body>

<?php include 'nav.php'; ?>

<main>
    <div class='container'>
        <div class='inline'>
        <h1>Interventions à venir</h1>
        <a href='intervention_add.php' class='btn btn--create'>Créer une nouvelle intervention</a>
        </div>
        <div class='responsive_table'>
            <table>
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Heure</th>
                    <th>Intitulé</th>
                    <th>Client</th>
                    <th>Employé(s)</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $req = $dbh->prepare($interventionRequest);
                $req->execute([ ':date' => $now ]);

                if ( $req->rowCount() > 0 ) {
                    while ( $intervention = $req->fetch() ) {
                        $timestamp = strtotime($intervention['date_inter']);
                        $date = date("d-m-Y", $timestamp);
                        $time = date("H:i", $timestamp);
                        ?>
                        <tr>
                            <td><?php echo $date; ?></td>
                            <td><?php echo $time; ?></td>
                            <td><?php echo $intervention['intitule']; ?></td>
                            <td><?php echo $intervention['nom_client']; ?></td>
                            <td>
                                <?php
                                $req2 = $dbh->prepare($employeRequest);
                                $req2->execute([ ':id_inter' => $intervention['id'] ]);

                                if ( $req2->rowCount() > 0 ) {
                                    echo "<ul>";
                                    while ( $employe = $req2->fetch() ) {
                                        echo "<li>". $employe['prenom'] . " " . $employe['nom'] . "</li>";
                                    }
                                    echo "</ul>";
                                }
                                ?>
                            </td>
                            <td>

                                <a href='intervention_view.php?inter=<?php echo $intervention['id']; ?>' class='btn
                                btn--icon btn--view'>
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="eye" class="svg-inline--fa fa-eye fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M288 144a110.94 110.94 0 0 0-31.24 5 55.4 55.4 0 0 1 7.24 27 56 56 0 0 1-56 56 55.4 55.4 0 0 1-27-7.24A111.71 111.71 0 1 0 288 144zm284.52 97.4C518.29 135.59 410.93 64 288 64S57.68 135.64 3.48 241.41a32.35 32.35 0 0 0 0 29.19C57.71 376.41 165.07 448 288 448s230.32-71.64 284.52-177.41a32.35 32.35 0 0 0 0-29.19zM288 400c-98.65 0-189.09-55-237.93-144C98.91 167 189.34 112 288 112s189.09 55 237.93 144C477.1 345 386.66 400 288 400z"></path></svg>
                                </a>
                                <a href='intervention_edit.php?inter=<?php echo $intervention['id']; ?>' class='btn
                                btn--icon btn--edit'>
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="edit" class="svg-inline--fa fa-edit fa-w-18" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M402.6 83.2l90.2 90.2c3.8 3.8 3.8 10 0 13.8L274.4 405.6l-92.8 10.3c-12.4 1.4-22.9-9.1-21.5-21.5l10.3-92.8L388.8 83.2c3.8-3.8 10-3.8 13.8 0zm162-22.9l-48.8-48.8c-15.2-15.2-39.9-15.2-55.2 0l-35.4 35.4c-3.8 3.8-3.8 10 0 13.8l90.2 90.2c3.8 3.8 10 3.8 13.8 0l35.4-35.4c15.2-15.3 15.2-40 0-55.2zM384 346.2V448H64V128h229.8c3.2 0 6.2-1.3 8.5-3.5l40-40c7.6-7.6 2.2-20.5-8.5-20.5H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V306.2c0-10.7-12.9-16-20.5-8.5l-40 40c-2.2 2.3-3.5 5.3-3.5 8.5z"></path></svg>
                                </a>
                                <a href='intervention_view.php?inter=<?php echo $intervention['id']; ?>&action=delete'
                                   class='btn btn--icon btn--delete'>
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>
                                </a>
                            </td>
                        </tr>

                        <?php
                    }
                } else {
                    echo "<tr><td colspan='6'>Il n'y pas d'interventions programmées.</td></tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</main>
</body>
</html>
