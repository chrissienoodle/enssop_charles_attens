<?php

$allEmpRequest = "SELECT id FROM employe";
$employeRequest = "SELECT id, nom, prenom FROM employe";
$insertInter = "INSERT INTO intervention(nom_client, date_inter, intitule, descr, duree) VALUE (:client, :date, :title, :descr, :duration)";
$insertEmp = "INSERT INTO inter_emp(id_emp, id_inter) VALUE (:idEmp, :idInter)";

$employees = [];
$error = false;
$error2 = false;
$empId = [];

require 'includes/validation.php';
require 'includes/connect.php';

//todo: générer une meilleure vérification des valeurs saisies par l'utilisateur (en particulier pour la date)
/*
 * documentation pour sanitiser les saisies utilisateurs :
 * https://html.spec.whatwg.org/multipage/input.html
 */
if ( isset($_POST['title']) && !empty($_POST['title'])
    && isset($_POST['date']) && !empty($_POST['date'])
    && isset($_POST['time']) && !empty($_POST['time'])
    && isset($_POST['duration']) && !empty($_POST['duration']) && intval($_POST['duration'] > 0)
    && isset($_POST['client']) && !empty($_POST['client'])
    && isset($_POST['employee']) && !empty($_POST['employee']) ) {

    //récupère tous les id des employés en base
    $req2 = $dbh->prepare($allEmpRequest);
    $req2->execute();

    //vérifie si les employés choisis par l'utilisateur existent en base
    if ( $req2->rowCount() > 0 ) {
        while ( $employeeId = $req2->fetch() ) {
            $empId[] = $employeeId['id'];
        }
        $i = 0;
        do {
            if ( in_array($_POST['employee'][$i], $empId) ) {
                $employees[] = $_POST['employee'][$i];
            }
            $i++;
        } while ( key_exists($i, $_POST['employee']) );
    }

    //insère l'intervention en base que si au moins un employé est correct
    if ( count($employees) ) {
        $client = $_POST['client'];
        $date = $_POST['date'] . " " . $_POST['time'] . ":00";
        $title = $_POST['title'];
        $description = isset($_POST['descr']) && !empty($_POST['descr']) ? $_POST['descr'] : null;
        $duration = intval($_POST['duration']);

        //insère l'intervention dans la table intervention
        $req3 = $dbh->prepare($insertInter);
        $req3->execute([ ':client' => $client,
                         ':date' => $date,
                         ':title' => $title,
                         ':descr' => $description,
                         ':duration' => $duration ]);
        $interId = $dbh->lastInsertId();

        //insère les valeurs dans la table inter_emp
        $req4 = $dbh->prepare($insertEmp);
        foreach ( $employees as $employee ) {
            $req4->execute([ ':idEmp' => $employee,
                             ':idInter' => $interId ]);
        }

        //renvoit sur la liste des interventions
        header("Location: index.php");
    } else {
        //todo: générer une meilleure gestion des erreurs.
        $error = true;
    }
} else {
    if ( isset($_POST['submit']) ) {
        $error2 = true;
    }
}
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Gestionnaire de prise de rendez-vous - Garage Attens</title>
</head>
<body>

<?php include 'nav.php'; ?>

<main>
    <div class='container'>
        <h1>Créer une nouvelle intervention</h1>

        <?php if ( $error ) { ?>
            <p class='error'>Les informations saisies ne sont pas conformes. L'intervention n'a pas été enregistrée.</p>
        <?php } ?>
        <?php if ( $error2 ) { ?>
            <p class='error'>Veuillez saisir l'ensemble des champs requis.</p>
        <?php } ?>

        <form action='intervention_add.php' method='post'>

            <label for='title'>Intitulé *</label><br>
            <input type='text' name='title' id='title' required><br>

            <label for='date'>Date *</label><br>
            <input type='date' name='date' id='date' required><br>

            <label for='time'>Heure *</label><br>
            <input type='time' name='time' id='time' required><br>

            <label for='duration'>Durée *</label><br>
            <input type='number' name='duration' id='duration' min='10' step='10' required><br>

            <label for='client'>Client *</label><br>
            <input type='text' name='client' id='client' required><br>

            <label for='descr'>Description</label><br>
            <textarea name='descr' id='descr' rows='10'></textarea><br>

            <?php
            //affiche dans un select tous les employés en base
            $req = $dbh->prepare($employeRequest);
            $req->execute();

            if ( $req->rowCount() > 0 ) {
                ?>
                <label for='employee'>Intervenant(s) *<br>
                    <small>(appuyer sur ctrl pour sélectionner plusieurs intervenants)</small>
                </label><br>
                <select name='employee[]' id='employee' multiple size='10' required>
                    <?php
                    while ( $employe = $req->fetch() ) {
                        echo "<option value='" . $employe['id'] . "'>" . $employe['prenom'] . " " . $employe['nom'] . "</option>";
                    }
                    ?>
                </select><br>
            <?php } ?>

            <input type='submit' name='submit' value='Sauvegarder'>
        </form>
    </div>
</main>
</body>
</html>
