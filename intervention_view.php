<?php

$interRequest = "SELECT * FROM intervention WHERE id = :idInter";
$employeRequest = "SELECT e.nom, e.prenom FROM employe AS e INNER JOIN inter_emp AS ie on e.id = ie.id_emp WHERE ie.id_inter = :idInter";
$deleteInter = "DELETE FROM intervention WHERE id = :idInter";
$deleteInterEmp = "DELETE FROM inter_emp WHERE id_inter = :idInter";

require 'includes/validation.php';
require 'includes/connect.php';

//renvoit sur la liste des interventions si l'adresse n'est pas correcte
if ( !isset($_GET['inter']) || empty($_GET['inter']) ) {
    header("Location: index.php");
}

$interId = $_GET['inter'];

$req = $dbh->prepare($interRequest);
$req->execute([ ':idInter' => $interId ]);

//renvoit sur la liste d'interventions si l'intervention n'existe pas
if ( $req->rowCount() < 0 ) {
    header("Location: index.php");
}

$intervention = $req->fetch();

//gère la suppression d'une intervention
if ( $_GET['action'] == 'delete' ) {
    $req3 = $dbh->prepare($deleteInterEmp);
    $req3->execute([ ':idInter' => $intervention['id'] ]);

    $req4 = $dbh->prepare($deleteInter);
    $req4->execute([ ':idInter' => $intervention['id'] ]);

    header("Location: index.php");
}

$timestamp = strtotime($intervention['date_inter']);
$date = date("d-m-Y", $timestamp);
$time = date("H:i", $timestamp);

$req2 = $dbh->prepare($employeRequest);
$req2->execute([ ':idInter' => $interId ]);
$employees = $req2->fetchAll();

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Gestionnaire de prise de rendez-vous - Garage Attens</title>
</head>
<body>

<?php include 'nav.php'; ?>

<main>
    <div class='container'>
        <h1>Fiche d'intervention n°<?php echo $intervention['id']; ?></h1>
        <a href='intervention_view.php?inter=<?php echo $intervention['id']; ?>&action=delete'>Supprimer</a>
        <a href='intervention_edit.php?inter=<?php echo $intervention['id']; ?>'>Modifier</a>


        <div>
            <label for='title'>Intitulé</label><br>
            <p id='title'><?php echo $intervention['intitule']; ?></p><br>

            <label for='date'>Date</label><br>
            <p id='date'><?php echo $date; ?></p><br>

            <label for='time'>Heure</label><br>
            <p id='date'><?php echo $time; ?></p><br>

            <label for='duration'>Durée</label><br>
            <p id='duration'><?php echo $intervention['duree']; ?> min</p><br>

            <label for='client'>Client</label><br>
            <p id='client'><?php echo $intervention['nom_client']; ?></p><br>

            <label for='descr'>Description</label><br>
            <p id='title'><?php echo $intervention['descr']; ?></p><br>

            <label>Intervenant<?php echo count($employees) > 1 ? "s" : ""; ?></label><br>
            <ul>
                <?php
                foreach ( $employees as $employe ) {
                    echo "<li class='employee'>" . $employe['prenom'] . " " . $employe['nom'] . "</li>";
                }
                ?>
            </ul>
            <br>
        </div>
    </div>
</main>
</body>
</html>
