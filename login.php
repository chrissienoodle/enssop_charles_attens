<?php

$loginRequest = "SELECT id, prenom, mdp, emp_role FROM employe WHERE login=:input OR mail=:input";

session_start();

require "includes/connect.php";

if ( isset($_POST['login']) && !empty($_POST['login']) && isset($_POST['pwd']) && !empty($_POST['pwd']) ) {
    $login = $_POST['login'];
    $pwd = $_POST['pwd'];

    $req = $dbh->prepare($loginRequest);
    $req->execute([ ':input' => $login ]);

    if ( $req->rowCount() > 0 ) {
        $result = $req->fetch();

        if ( password_verify($pwd, $result['mdp']) && in_array($result['emp_role'], [ 1,
                                                                                      2 ]) ) {
            $_SESSION['role'] = ( $result['emp_role'] == 1 ) ? 'admin' : 'employe';
            header("Location: index.php");
        }
    }
}

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Connexion - Garage Attens</title>
</head>
<body>
<main id='loginPage'>
    <div class='container'>
        <h1>Espace de connexion au gestionnaire de rendez-vous</h1>
        <form action='login.php' method='post'>
            <?php
            if ( isset($_POST['submit']) && empty($_POST['login']) ) {
                echo '<p class="error">Veuillez saisir un identifiant ou une adresse email valide</p>';
            }
            if ( isset($_POST['submit']) && empty($_POST['pwd']) ) {
                echo '<p class="error">Veuillez saisir votre mot de passe</p>';
            }
            ?>
            <label for='login'>Identifiant ou adresse email</label><br>
            <input type='text' name='login' id='login' required><br>

            <label for='pwd'>Mot de passe</label><br>
            <input type='password' name='pwd' id='pwd' required><br>
            <input class='btn btn--create' type='submit' name='submit' value='Se Connecter'>
        </form>
    </div>
</main>
</body>
</html>
