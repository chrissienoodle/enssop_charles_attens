<h1>TP Charles Attens</h1>
Proposition de correction du TP du 16/03/20
<br>
<h2>Import de la base de données</h2>
La base de données s'appelle <strong>enssop_correction_attens</strong>.
 La structure de la base de données se trouve dans le fichier dbDump.sql<br><br>

<strong>Vous devez modifier les mots de passe des employés dans le fichier dbDump</strong>

Saisir une des commandes suivantes dans un terminal pour executer les requetes présentes dans le fichier dbDump
.sql :<br>
<code>mysql < dbDump.sql</code><br><br>
pour spécifier un nom d'utilisateur et un mot de passe : <br>
<code>mysql -u nomdutilisateur -p < database.sql</code>

<h2>Configuration du projet</h2>
Pour faire fonctionner le projet, vous devez créer un fichier appelé <code>config.php</code>
dans le répertoire <code>includes</code>qui devra contenir les informations suivantes : <br><br>
```
<?php

$db_host = "";
$db_name = "";
$db_user = "";
$db_pass = "";
``` 

