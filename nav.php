<?php
?>
<nav>
    <ul>
        <li><span>Garage Attens</span></li>
        <?php if ($_SESSION['role'] === 'admin'){ ?>
            <li>
                <span>Gestion des employés</span>
                <ul>
                    <li><a href='employes.php'>Liste des employés</a></li>
                </ul>
            </li>
        <?php } ?>
        <li>
            <span>Gestion des interventions</span>
            <ul>
                <li><a href='index.php'>Liste des interventions à venir</a></li>
                <li><a href='intervention_add.php'>Créer une nouvelle intervention</a></li>
            </ul>
        </li>
    </ul>
</nav>
