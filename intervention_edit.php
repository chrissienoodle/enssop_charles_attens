<?php

$allEmpRequest = "SELECT id FROM employe";
$employeRequest = "SELECT id, nom, prenom FROM employe";
$interRequest = "SELECT * FROM intervention WHERE id = :idInter";
$interEmpRequest = "SELECT * FROM inter_emp WHERE id_inter = :idInter";
$updateInter = "UPDATE intervention SET nom_client = :client, date_inter = :date, intitule = :title, descr = :descr, duree = :duration WHERE id = :idInter";
$deleteEmp = "DELETE FROM inter_emp WHERE id_emp = :idEmp AND id_inter = :idInter";
$insertEmp = "INSERT INTO inter_emp(id_emp, id_inter) VALUE (:idEmp, :idInter)";

$employees = [];
$error = false;
$error2 = false;
$empId = [];
$empInDb = [];

require 'includes/validation.php';
require 'includes/connect.php';

//todo: déporter le code dupliqué dans une fonction
//renvoit sur la liste des interventions si l'adresse n'est pas correcte
if ( !isset($_GET['inter']) || empty($_GET['inter']) ) {
    header("Location: index.php");
}

$interId = $_GET['inter'];

$req = $dbh->prepare($interRequest);
$req->execute([ ':idInter' => $interId ]);

//renvoit sur la liste d'interventions si l'intervention n'existe pas
if ( $req->rowCount() < 0 ) {
    header("Location: index.php");
}

$intervention = $req->fetch();

$timestamp = strtotime($intervention['date_inter']);
$date = date("Y-m-d", $timestamp);
$time = date("H:i", $timestamp);

$req2 = $dbh->prepare($interEmpRequest);
$req2->execute([ ':idInter' => $interId ]);
while ( $emps = $req2->fetch() ) {
    $empInDb[] = $emps['id_emp'];
}

//todo: générer une meilleure vérification des valeurs saisies par l'utilisateur
/*
 * documentation pour sanitiser les saisies utilisateurs :
 * https://html.spec.whatwg.org/multipage/input.html
 */
if ( isset($_POST['title']) && !empty($_POST['title'])
    && isset($_POST['date']) && !empty($_POST['date'])
    && isset($_POST['time']) && !empty($_POST['time'])
    && isset($_POST['duration']) && !empty($_POST['duration']) && intval($_POST['duration'] > 0)
    && isset($_POST['client']) && !empty($_POST['client'])
    && isset($_POST['employee']) && !empty($_POST['employee']) ) {

    //récupère tous les id des employés en base
    $req3 = $dbh->prepare($allEmpRequest);
    $req3->execute();

    //vérifie si les employés choisis par l'utilisateur existent en base
    if ( $req3->rowCount() > 0 ) {
        while ( $employeeId = $req3->fetch() ) {
            $empId[] = $employeeId['id'];
        }
        $i = 0;
        do {
            if ( in_array($_POST['employee'][$i], $empId) ) {
                $employees[] = $_POST['employee'][$i];
            }
            $i++;
        } while ( key_exists($i, $_POST['employee']) );
    }

    //modifie l'intervention en base que si au moins un employé est correct
    if ( count($employees) ) {
        $client = $_POST['client'];
        $date = $_POST['date'] . " " . $_POST['time'] . ":00";
        $title = $_POST['title'];
        $description = isset($_POST['descr']) && !empty($_POST['descr']) ? $_POST['descr'] : null;
        $duration = intval($_POST['duration']);


        //insère l'intervention dans la table intervention
        $req4 = $dbh->prepare($updateInter);
        $req4->execute([ ':client' => $client,
                         ':date' => $date,
                         ':title' => $title,
                         ':descr' => $description,
                         ':duration' => $duration,
                         ':idInter' => $intervention['id'] ]);

        //supprime les enregistrements dans la table inter_emp pour un employé n'étant plus affecté à une tache
        $req5 = $dbh->prepare($deleteEmp);
        foreach ( $empInDb as $emp ) {
            if ( !in_array($emp, $employees) ) {
                $req5->execute([ ':idEmp' => $emp,
                                 ':idInter' => $interId ]);
            }
        }

        //insère les enregistrement dans la table inter_emp pour un employé nouvellement affecté à une intervention
        $req6 = $dbh->prepare($insertEmp);
        foreach ( $employees as $emp ) {
            if ( !in_array($emp, $empInDb) ) {
                $req6->execute([ ':idEmp' => $emp,
                                 ':idInter' => $interId ]);
            }
        }

        //renvoit sur la liste des interventions
        header("Location: index.php");
    } else {
        //todo: générer une meilleure gestion des erreurs.
        $error = true;
    }
} else {
    if ( isset($_POST['submit']) ) {
        $error2 = true;
    }
}

?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/reset.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Gestionnaire de prise de rendez-vous - Garage Attens</title>
</head>
<body>

<?php include 'nav.php'; ?>

<main>
    <div class='container'>
        <h1>&Eacute;dition de la fiche d'intervention n°<?php echo $intervention['id']; ?></h1>

        <?php if ( $error ) { ?>
            <p class='error'>Les informations saisies ne sont pas conformes. L'intervention n'a pas été modifiée.</p>
        <?php } ?>
        <?php if ( $error2 ) { ?>
            <p class='error'>Veuillez saisir l'ensemble des champs requis.</p>
        <?php } ?>

        <form action='intervention_edit.php?inter=<?php echo $intervention['id']; ?>' method='post'>

            <label for='title'>Intitulé *</label><br>
            <input type='text' name='title' id='title' required value='<?php echo $intervention['intitule']; ?>'><br>

            <label for='date'>Date *</label><br>
            <input type='date' name='date' id='date' required value='<?php echo $date; ?>'><br>

            <label for='time'>Heure *</label><br>
            <input type='time' name='time' id='time' required value='<?php echo $time; ?>'><br>

            <label for='duration'>Durée *</label><br>
            <input type='number' name='duration' id='duration' step='10' required
                   value='<?php echo $intervention['duree'];
                   ?>'><br>

            <label for='client'>Client *</label><br>
            <input type='text' name='client' id='client' required value='<?php echo $intervention['nom_client'];
            ?>'><br>

            <label for='descr'>Description</label><br>
            <textarea name='descr' id='descr' rows='10'><?php echo $intervention['descr'];
                ?></textarea><br>

            <?php
            //affiche dans un select tous les employés en base
            $req = $dbh->prepare($employeRequest);
            $req->execute();

            if ( $req->rowCount() > 0 ) {
                ?>
                <label for='employee'>Intervenant(s) *<br>
                    <small>(appuyer sur ctrl pour sélectionner plusieurs intervenants)</small>
                </label><br>
                <select name='employee[]' id='employee' multiple size='10' required>
                    <?php
                    while ( $employe = $req->fetch() ) {
                        $isSelected = in_array($employe['id'], $empInDb) ? "selected" : "";
                        echo "<option value='" . $employe['id'] . "' " . $isSelected . ">" . $employe['prenom'] . " " . $employe['nom'] . "</option>";
                    }
                    ?>
                </select><br>
            <?php } ?>

            <input type='submit' name='submit' value='Sauvegarder'>
        </form>

        <a href='intervention_view.php'>Annuler</a>
        <a href='index.php'>Retour à la liste d'interventions</a>
    </div>
</main>
</body>
</html>
