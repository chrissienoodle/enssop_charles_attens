CREATE DATABASE IF NOT EXISTS enssop_correction_attens;

USE enssop_correction_attens;

CREATE TABLE IF NOT EXISTS employe
(
    id       SMALLINT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nom      VARCHAR(255) NOT NULL,
    prenom   VARCHAR(255) NOT NULL,
    adresse  VARCHAR(255),
    cp       CHAR(5),
    ville    VARCHAR(255),
    tel      VARCHAR(15),
    mail     VARCHAR(255) NOT NULL UNIQUE,
    login    VARCHAR(255) NOT NULL UNIQUE,
    mdp      VARCHAR(255) NOT NULL,
    emp_role TINYINT      NOT NULL,
    date_emb DATE         NOT NULL,
    CONSTRAINT ck_role CHECK ( emp_role = 1 OR emp_role = 2)
);

INSERT INTO employe(nom, prenom, adresse, cp, ville, tel, mail, login, mdp, emp_role, date_emb)
    VALUES ('Attens', 'Charles', 'route des Jasnières', '72340', 'La Chartre-sur-le-Loir', '0606060606',
            'charles@attens.fr', 'charles', '$2y$10$g0JdamD6UYcW.6l3qD.aleWVDb/cj7y8oXlsLqexaHn2SYXmJJNm2', 1, '27/02/1997'),
           ('Attens', 'John', 'chemin de la Poterie', '72340', 'La Chartre-sur-le-Loir', '0707070707',
            'john@attens.fr','john', '$2y$10$1yQIvCNPuic6nvpkrW0HG.8H2K9L7wC0q1Qa/FIqCFWnk4ZbnlbU6', 1, '27/02/1997'),
           ('Dupont', 'Jean', null, null, null, null, 'j.dupont@attens.fr', 'jean',
            '$2y$10$Zps6eaZCOdH3v5ZEYAn6W.NHQnTAymRpg4G8bEDV4LmaJQE17403a', 2, '01/03/1997'),
           ('Pineau', 'Patrick', null, null, null, '+33601020304', 'p.pineau@attens.fr', 'patrick',
            '$2y$10$Bt8qcUeXEAG9IC.ghD.62e8kkgld6T0V4wIC6pElkHM7MIh3dnNQu', 2, '30/03/1997'),
           ('Ramond', 'Georges', null, null, null, null, 'g.ramon@attens.fr', 'georges',
            '$2y$10$.gH9NhduijfgkSt0lM1ONe23.RCjLEqZu6o8Yfl6xNj5Z65.OwJtK', 2, '07/09/1997');

CREATE TABLE IF NOT EXISTS intervention
(
    id         INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    nom_client VARCHAR(255) NOT NULL,
    date_inter DATETIME     NOT NULL,
    intitule   VARCHAR(255) NOT NULL,
    descr      TEXT,
    duree      SMALLINT     NOT NULL
);

INSERT INTO intervention(nom_client, date_inter, intitule, descr, duree)
    VALUES ('Maryse Duhammel', '2020:03:01 09:00:00', 'vidange', null, '40'),
           ('Christian Lhomme', '2020:04:01 10:20:00', 'pneus',
            'changement des pneus avant sur une peugeot 206 immatriculée DK-258-BG', '70');

CREATE TABLE IF NOT EXISTS inter_emp
(
    id_emp   SMALLINT   UNSIGNED,
    id_inter INT        UNSIGNED,
    CONSTRAINT fk_emp
        FOREIGN KEY (id_emp) REFERENCES employe(id)
        ON DELETE SET NULL
        ON UPDATE CASCADE,
    CONSTRAINT fk_inter
        FOREIGN KEY (id_inter) REFERENCES intervention(id)
        ON DELETE SET NULL
        ON UPDATE CASCADE
);

INSERT INTO inter_emp(id_emp, id_inter)
    VALUES ('1', '1'),
           ('3', '1'),
           ('2', '2');
